#pragma once

#include "ofMain.h"
#include "ofxGui.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		
		void getData();

		ofTrueTypeFont ttf;
		ofJson	js;

		ofVideoPlayer	video;
		
		ofPixels videoPixels;
		ofTexture videoTexture;
		
		ofxPanel gui;
		ofxFloatSlider videoTime;

		int time;
		int x1, x2, y1, y2;
		float score;

		bool bHide;
		bool toggleTracking;

};
