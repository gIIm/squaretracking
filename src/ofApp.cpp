#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	ofSetBackgroundColor(0);
	ofSetFrameRate(30);
	ofSetVerticalSync(true);
	ofEnableSmoothing();

	// FONT
	ttf.load("font/mono.ttf", 8);

	// JSON
	ofFile file("json/darts-sample.json");
	if (file.exists()) {
		file >> js;
	}

	// getData();

	// VIDEO
	video.load("video/darts.mp4");
	video.setLoopState(OF_LOOP_NONE);
	video.play();
	//video.setPaused(true);

	// 2554 x 1080
	videoPixels.allocate(480, 360, OF_PIXELS_RGB);
	videoTexture.allocate(videoPixels);

	time = 0;
	x1 = 0;
	y1 = 0;
	x2 = 0;
	y2 = 0;
	score = 0;

	// GUI
	gui.setup();
	gui.setPosition(700, 0);
	gui.add(videoTime.setup("time", 0.0, 0.0, 124.0));

	bHide = false;

	toggleTracking = false;

}

//--------------------------------------------------------------
void ofApp::update(){
	//video.setPosition(videoTime/60.0);
	//video.update();

	if (toggleTracking) {
		auto& data = js;

		for (auto i = 0; i < data.size(); i++) {
			if (data[i]["time"].size() != 0) {
				time = data[i]["time"];
				video.setPosition(time/100);
				video.update();
			}

			if (data[i]["results"].size() != 0 && video.isFrameNew()) {

				auto results = data[i]["results"][0];

				if (results["bbox"].size() != 0) {
					x1 = results["bbox"][0];
					y1 = results["bbox"][1];
					x2 = results["bbox"][2];
					y2 = results["bbox"][3];
				}

				if (results["class"].size() != 0) {
					std::string category = results["class"];
				}

				if (results["score"].size() != 0) {
					score = results["score"];
				}

			}

		}

		if (video.isFrameNew()) {
			ofPixels & pixels = video.getPixels();
			for (size_t i = 0; i < pixels.size(); i++) {
				//invert the color of the pixel
				//videoPixels[i] = 255 - pixels[i];
				videoPixels[i] = pixels[i];
			}
			//load the inverted pixels
			videoTexture.loadData(videoPixels);
		}
	
	}
}

//--------------------------------------------------------------
void ofApp::draw(){
	ofSetColor(255);

	video.draw(0, 0);

	videoTexture.draw(0, 360);
	
	int width = x1 - x2;
	int height = y2 - y1;

	glm::vec2 p;
	p.x = x2 + width / 2;
	p.y = y1 + height / 2;

	ofRectangle myRect;
	myRect.setFromCenter(p, width, height);

	ofSetColor(255, 0, 0);
	ofNoFill();
	ofDrawRectRounded(myRect, 10);

	ofSetColor(255, 0, 127);

	ofDrawBitmapString("frame: " + ofToString(video.getCurrentFrame()) + "/" + ofToString(video.getTotalNumFrames()), 500, 20);
	ofDrawBitmapString("duration: " + ofToString(video.getPosition()*video.getDuration(), 2) + "/" + ofToString(video.getDuration(), 2), 500, 40);
	ofDrawBitmapString("speed: " + ofToString(video.getSpeed(), 2), 500, 60);

	if (video.getIsMovieDone()) {
		ofSetColor(255, 0, 0);
		ofDrawBitmapString("end of movie", 500, 80);
	}

	if (!bHide) {
		gui.draw();
	}
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

	if (key == OF_KEY_RETURN) toggleTracking = !toggleTracking;

	if (key == 'h') {
		bHide = !bHide;
	}
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

//--------------------------------------------------------------
void ofApp::getData() {
	auto& data = js;

	for (auto i = 0; i < data.size(); i++) {
		//cout << data[i] << endl;
		
		if (data[i]["time"].size() != 0) {
			int time = data[i]["time"];
		}

		if (data[i]["results"].size() != 0) {

			auto results = data[i]["results"][0];

			if (results["bbox"].size() != 0) {
				int x1 = results["bbox"][0];
				int y1 = results["bbox"][1];
				int x2 = results["bbox"][2];
				int y2 = results["bbox"][3];
			}

			if (results["class"].size() != 0) {
				string category = results["class"];
			}

			if (results["score"].size() != 0) {
				float score = results["score"];
			}

		}

	}

}
